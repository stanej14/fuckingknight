/* 
 * File:   GameFactory.h
 * Author: honza
 *
 * Created on October 8, 2015, 11:24 AM
 */

#ifndef GAMEBUILDER_H
#define	GAMEBUILDER_H

#include "Game.h"

class GameBuilder {
public:
    GameBuilder();
    GameBuilder setBoardSize(int size);
    GameBuilder setKnightCoordinates(int row, int col);
    GameBuilder setDensity(float density);
    Game* build();
protected:
    int generateBoard(int** board);
private:
    int mSize;
    int mKnightRow;
    int mKnightCol;
    float mDensity;
};

#endif	/* GAMEFACTORY_H */

