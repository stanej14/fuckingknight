/* 
 * File:   FileExporter.cpp
 * Author: honza
 * 
 * Created on October 8, 2015, 11:59 AM
 */

#include "FileExporter.h"
#include "ChessBoard.h"
#include <iostream>
#include <fstream>

using namespace std;

FileExporter::FileExporter() {
    input = "input.txt";
    output = "output.txt";
}

void FileExporter::writeInitialStateToFile(Game* game) {
    ChessBoard* chessBoard = game->getBoard();
    int** field = chessBoard->getField();
    ofstream outFile;
    outFile.open(input);
    for(int row = 0; row < chessBoard->getSize(); ++row) {
        for(int col = 0; col < chessBoard->getSize(); ++col) {
                  
            outFile << field[row][col];
            outFile << " | ";
        }
        outFile << endl;
    }
    outFile.close();
}

void FileExporter::writeResultToFile(Game* game) {
    ofstream outFile;
    int size = game->getBoard()->getSize();
    int** field = game->getBoard()->getField();
    outFile.open(output);
    for(int row = 0; row < size; ++row) {
        for(int col = 0; col < size; ++col) {
            
            if(field[row][col] == -2) {
                field[row][col] = game->getCurrentJumps();
            }
            
            outFile << field[row][col] << " | ";
        }
        outFile << endl;
    }
    outFile.close();
}
