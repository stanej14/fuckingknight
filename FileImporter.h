/* 
 * File:   FileImport.h
 * Author: krata
 *
 * Created on October 11, 2015, 5:56 PM
 */

#ifndef FILEIMPORTER_H
#define	FILEIMPORTER_H

#include "Game.h"
#include <string>
#include <fstream>

class FileImporter {
public:
    FileImporter();
    //Game* readInitialState();
    Game* createGame();
private:
    void openFile();
    void closeFile();
    int parseLine(int*& boolLine, Knight*& knight, string str, int row, int &figureCount);
    bool* createBoolArray(vector<bool> boolVector);
    Game* parseGame();
    
    const char* input;
    ifstream inFile;

};

#endif	/* FILEIMPORT_H */

