/* 
 * File:   Game.cpp
 * Author: honza
 * 
 * Created on October 7, 2015, 1:15 PM
 */

#include <iostream>
#include <algorithm>
#include "Game.h"
#include <stdio.h>

using namespace std;

Game::Game(Knight* knight, ChessBoard* board) {
    mKnight = knight;
    mBoard = board;

    int size = mBoard->getSize();
    mUpperBound = size * size - 1;
    mCurrentJumps = 0;
}

Game::~Game() {
    delete mKnight;
    delete mBoard;
    mGameManager = NULL;
}

/**
 * Starts the game
 * @return number of moves
 */
int Game::start() {
    while (true) {

        if (mBoard->isEmpty()) {
            return mCurrentJumps;
        } else if (!canFinishBeforeTheLimit()) {
            return mUpperBound + 2;
        }

        Point* p = calculateBestCoordinate();
        moveKnightTo(p);
        delete p;
    }
}

/**
 * Compares two coordinates by their jumpPrice ascending
 * @param a
 * @param b
 * @return jumpPrice(a) < jumpPrice(b)
 */

bool Game::compareCoordinates(Point* a, Point* b) {
    return jumpPrice(a) < jumpPrice(b);
}

/**
 * Changes coordinates of the knight to given point
 * @param p next coordinates
 */
void Game::moveKnightTo(Point* p) {
    mBoard->setPointValue(mKnight,mCurrentJumps++);
    mKnight->setCol(p->getCol());
    mKnight->setRow(p->getRow());
    mBoard->removeFigure(p);
    mBoard->setPointValue(mKnight,-2);
}

/**
 * Gets the best coordinate to jump on
 * @return Point
 */
Point* Game::calculateBestCoordinate() {
    vector<Point*> points;
    calculatePossibleCoordinates(points, mKnight);

    vector<Point*>::iterator it;

    // Sorts points according to their jump price
    // TODO
    // sort(points.begin(),points.end(),compareCoordinates);
    vector<Point*> sortedPoints;
    sortedPoints.push_back(points.back());
    points.pop_back();
    bool inserted;
    while (!points.empty()) {
        Point* temp = points.back();
        points.pop_back();

        inserted = false;
        for (it = sortedPoints.begin(); it != sortedPoints.end(); ++it) {
            Point* sortedPoint = (*it);
            if (compareCoordinates(temp, sortedPoint)) {
                sortedPoints.insert(it, temp);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            sortedPoints.push_back(temp);
        }
    }

    Point* toReturn = sortedPoints.back();
    sortedPoints.pop_back();

    // Adds other options where to go to the stack
    while (!sortedPoints.empty()) {
        Point* point = sortedPoints.back();
        sortedPoints.pop_back();

        Game *temp = deepCopy();
        temp->moveKnightTo(point);
        if (temp->canFinishBeforeTheLimit()) {
            mGameManager->addGame(temp);
        } else {
            delete temp;
        }
        delete point;
    }

    return toReturn;
}

bool Game::canFinishBeforeTheLimit() {
    return mCurrentJumps + mBoard->getFigureCount() <= mUpperBound;
}

Game* Game::deepCopy() {

    // Deep copies Knight
    Knight* knight = new Knight(mKnight->getRow(), mKnight->getCol());

    // Deep copies ChessBoard
    int size = mBoard->getSize();
    int** origField = mBoard->getField();
    int** field = new int*[size];
    for (int i = 0; i < size; ++i) {
        field[i] = new int[size];
    }
    for (int row = 0; row < size; ++row) {
        for (int col = 0; col < size; ++col) {
            field[row][col] = origField[row][col];
        }
    }
    ChessBoard *board = new ChessBoard(size, field, mBoard->getFigureCount());
   
    Game *toReturn = new Game(knight, board);
    toReturn->setCurrentJumps(mCurrentJumps);
    toReturn->setGameManager(mGameManager);
    return toReturn;
}

/**
 * Calculates price of a jump to the given point
 * @param p point
 * @return price
 */
int Game::jumpPrice(Point* p) {
    int val = 0;
    if (mBoard->isOccupied(p)) {
        val = 8;
    }

    vector<Point*> points;
    calculatePossibleCoordinates(points, p);

    for (int i = 0; i < points.size(); ++i) {
        if (mBoard->isOccupied(points.at(i))) {
            ++val;
        }
    }

    while (!points.empty()) {
        Point* point = points.back();
        points.pop_back();
        delete point;
    }
    return val;
}

/**
 * Gets every possible coordinate to move to from given point
 * @return vector of coordinates
 */
void Game::calculatePossibleCoordinates(vector<Point*> &points, Point* point) {
    int row = point->getRow();
    int col = point->getCol();
    addPointIfValid(points, row + 2, col + 1);
    addPointIfValid(points, row + 2, col - 1);
    addPointIfValid(points, row - 2, col + 1);
    addPointIfValid(points, row - 2, col - 1);
    addPointIfValid(points, row + 1, col + 2);
    addPointIfValid(points, row + 1, col - 2);
    addPointIfValid(points, row - 1, col + 2);
    addPointIfValid(points, row - 1, col - 2);
}

/**
 * Adds new point to the vector if coordinates are valid
 * @param points
 * @param row
 * @param col
 */
void Game::addPointIfValid(vector<Point*> &points, int row, int col) {
    if (mBoard->isValidCoordinate(row, col)) {
        points.push_back(new Point(row, col));
    }
}

void Game::setGameManager(GameManager* manager) {
    mGameManager = manager;
}

void Game::setCurrentJumps(int currentJumps) {
    mCurrentJumps = currentJumps;
}

void Game::setUpperBound(int upperBound) {
    mUpperBound = upperBound;
}

ChessBoard* Game::getBoard() {
    return mBoard;
}

Knight* Game::getKnight() {
    return mKnight;
}

int Game::getCurrentJumps() {
    return mCurrentJumps;
}

GameManager* Game::getGameManager() {
    return mGameManager;
}