/* 
 * File:   FileExporter.h
 * Author: honza
 *
 * Created on October 8, 2015, 11:59 AM
 */

#ifndef FILEEXPORTER_H
#define	FILEEXPORTER_H

#include "Game.h"

class FileExporter {
public:
    FileExporter();
    void writeInitialStateToFile(Game* game);
    void writeResultToFile(Game* game);
private:
    const char* input;
    const char* output;
};

#endif	/* FILEEXPORTER_H */

