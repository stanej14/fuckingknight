/* 
 * File:   ChessBoard.cpp
 * Author: honza
 * 
 * Created on October 7, 2015, 1:02 PM
 */

#include "ChessBoard.h"
#include "Point.h"
#include <iostream>

using namespace std;

ChessBoard::ChessBoard(int size, int** field, int figureCount) {
    mSize = size;
    mField = field;
    mFigureCount = figureCount;
}

ChessBoard::~ChessBoard() {
    for (int i = 0; i < mSize; ++i) {
        delete[] mField[i];
    }
    delete[] mField;
}

int** ChessBoard::getField() const {
    return mField;
}

int ChessBoard::getFigureCount() const {
    return mFigureCount;
}

int ChessBoard::getSize() const {
    return mSize;
}

void ChessBoard::removeFigure(Point* p) {
    if (isValidCoordinate(p->getRow(), p->getCol())) {
        if (isOccupied(p)) {
            mField[p->getRow()][p->getCol()] = 0;
            --mFigureCount;
        }
    }
}

void ChessBoard::setPointValue(Point* p, int value){
    if (isValidCoordinate(p->getRow(), p->getCol())) {
        mField[p->getRow()][p->getCol()] = value;
    }
}

bool ChessBoard::isEmpty() const {
    return mFigureCount <= 0;
}

bool ChessBoard::isOccupied(Point *p) const {
    return mField[p->getRow()][p->getCol()] == -1;
}

bool ChessBoard::isOccupied(int row, int col) const {
    return mField[row][col] == -1;
}

bool ChessBoard::isValidCoordinate(int row, int col) const {
    return !(row >= mSize || col >= mSize || row < 0 || col < 0);
}

