/* 
 * File:   GameManager.h
 * Author: honza
 *
 * Created on October 8, 2015, 9:24 PM
 */

#ifndef GAMEMANAGER_H
#define	GAMEMANAGER_H

#include <stack>

using namespace std;

class Game;

class GameManager {
    
public:
    GameManager();
    ~GameManager();
    void addGame(Game* game);
    Game* takeGameFromStack();
private:
    stack<Game*> mGameStack;
};

#endif	/* GAMEMANAGER_H */

