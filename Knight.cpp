/* 
 * File:   Knight.cpp
 * Author: honza
 * 
 * Created on October 7, 2015, 1:12 PM
 */

#include "Knight.h"

Knight::Knight(int row, int col) : Point(row,col){
}
 
 void Knight::setRow(int row) {
     mRow = row;
 }
 
 void Knight::setCol(int col) {
     mCol = col;
 }
