/* 
 * File:   Knight.h
 * Author: honza
 *
 * Created on October 7, 2015, 1:12 PM
 */

#ifndef KNIGHT_H
#define	KNIGHT_H

#include "Point.h"


class Knight : public Point {
public:
    Knight(int row, int col);
    void setRow(int row);
    void setCol(int col);
};

#endif	/* KNIGHT_H */

