/* 
 * File:   FileImport.cpp
 * Author: krata
 * 
 * Created on October 11, 2015, 5:56 PM
 */

#include "FileImporter.h"
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

FileImporter::FileImporter() {
    input = "input.txt";
}

void FileImporter::openFile() {
    inFile.open(input);
}

void FileImporter::closeFile() {
    inFile.close();
}

Game* FileImporter::createGame() {
    openFile();
    Game* game = parseGame();
    closeFile();
    return game;
}

Game* FileImporter::parseGame() {
    string str;
    int** board = NULL, * boolLine = NULL;
    Knight* knight = NULL;
    int row = 0, col;
    int figureCount = 0;
    while(getline(inFile, str)) {
        col = parseLine(boolLine, knight, str, row, figureCount);
        if(!board) {
            board = new int*[col];
        }
        board[row] = boolLine;
        ++row;
    };
    return new Game(knight, new ChessBoard(row, board, figureCount));
}

int FileImporter::parseLine(int*& boolLine, Knight*& knight, string str, int row, int &figureCount) {
    vector<int> intVector;
    int col = 0;
//    for(int i = 0; i < str.length(); ++i) {
//        if(str.at(i) == -1) {
//            intVector.push_back(-1);
//            ++col;
//        }else if(str.at(i) == 'K') {
//            intVector.push_back(false);
//            knight = new Knight(row, col);
//            ++col;
//        }else if(str.at(i) == '+') {
//            ++figureCount;
//            intVector.push_back(true);
//            ++col;
//        }
//    }
//    boolLine = createBoolArray(intVector);
    // TODO
    return col;
}

bool* FileImporter::createBoolArray(vector<bool> boolVector) {
    int lineSize = boolVector.size();
    bool* boolLine = new bool[lineSize];
    for(int i = 0; i < lineSize; ++i) {
        boolLine[i] = boolVector.at(i);
    }
    return boolLine;
}
