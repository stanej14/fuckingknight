#ifndef POINT_H
#define	POINT_H

class Point {
public:
    Point(int row, int col);
    int getRow();
    int getCol();
protected:
    int mRow;
    int mCol;
};

#endif	/* POINT_H */

