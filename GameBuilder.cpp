/* 
 * File:   GameFactory.cpp
 * Author: honza
 * 
 * Created on October 8, 2015, 11:24 AM
 */

#include "GameBuilder.h"
#include "Knight.h"
#include "ChessBoard.h"
#include "Game.h"
#include <cstdlib>
#include <ctime>

GameBuilder::GameBuilder() {
    this->mSize = 10;
    this->mKnightRow = 0;
    this->mKnightCol = 0;
    this->mDensity = 0.5;
}

GameBuilder GameBuilder::setBoardSize(int size) {
    this->mSize = size;
    return *this;
}

GameBuilder GameBuilder::setKnightCoordinates(int row, int col) {
    this->mKnightRow = row;
    this->mKnightCol = col;
    return *this;
}

GameBuilder GameBuilder::setDensity(float density) {
    this->mDensity = density;
    return *this;
}

Game* GameBuilder::build() {
    Knight *knight = new Knight(mKnightRow, mKnightCol);

    int** field = new int*[mSize];
    for (int i = 0; i < mSize; ++i) {
        field[i] = new int[mSize];
    }
    int figureCount = generateBoard(field);
    
    // Sets knight's position
    if(field[knight->getRow()][knight->getCol()] == -1) {
        --figureCount;
    }
    field[knight->getRow()][knight->getCol()] = -2;
    
    
    ChessBoard* board = new ChessBoard(mSize, field, figureCount);
    return new Game(knight, board);
}

/**
 * Generates board with figures
 * @param board
 * @return number of figures
 */
int GameBuilder::generateBoard(int** board) {

    int figureCount = 0;

    // Initialize random seed
    srand(time(NULL));
    int compareTo = (int) ((1 - mDensity) * 100);

    // Sets randomly figures to the 
    for (int row = 0; row < mSize; ++row) {
        for (int col = 0; col < mSize; ++col) {
            bool putFigure = (rand() % 100) >= compareTo;
            if (putFigure) {
                ++figureCount;
                board[row][col] = -1;
            } else {
                board[row][col] = 0;
            }
        }
    }
    return figureCount;
}
