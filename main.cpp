/* 
 * File:   main.cpp
 * Author: honza
 *
 * Created on October 10, 2015, 1:45 PM
 */

#include <cstdlib>
#include <stdio.h>
#include <iostream>

#include "GameBuilder.h"
#include "FileExporter.h"
#include "FileImporter.h"

using namespace std;

int main(int argc, char** argv) {

//    FileImporter fileImporter;
//    Game* bestApproach = fileImporter.readInitialState();
    
    GameBuilder builder;
    Game* bestApproach = builder
            .setBoardSize(4)
            .setDensity(0.5)
            .setKnightCoordinates(0,0)
            .build();
    GameManager *manager = new GameManager();
    bestApproach->setGameManager(manager);
    
    int startingFigureCount = bestApproach->getBoard()->getFigureCount();
    
    
    FileExporter fileExporter;
    fileExporter.writeInitialStateToFile(bestApproach);
    
    int bestPossibleMoveCount = bestApproach->getBoard()->getFigureCount();
    int bestApproachMoveCount = bestApproach->start();
    int currentApproachMoveCount;
    while(true) {
        
        if(bestApproachMoveCount <= bestPossibleMoveCount) {
            break;
        }
        
        Game *currentApproach = manager->takeGameFromStack();
        if(!currentApproach) {
            break;
        }
        currentApproach->setUpperBound(bestApproachMoveCount - 1);

        currentApproachMoveCount = currentApproach->start();
        if(currentApproachMoveCount < bestApproachMoveCount) {
            bestApproachMoveCount = currentApproachMoveCount;
            delete bestApproach;
            bestApproach = currentApproach;
            printf("New best = %d",bestApproachMoveCount);
        } else {
            delete currentApproach;
        }
    }
    printf("Best approach has %d jumps",bestApproachMoveCount);

    fileExporter.writeResultToFile(bestApproach);
    
    delete bestApproach;
    delete manager;
    return 0;
}

