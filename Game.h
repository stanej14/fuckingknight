/* 
 * File:   Game.h
 * Author: honza
 *
 * Created on October 7, 2015, 1:15 PM
 */

#ifndef GAME_H
#define	GAME_H

#include "Knight.h"
#include "ChessBoard.h"
#include "GameManager.h"
#include <vector>

using namespace std;

class Game {
public:
    Game(Knight* knight, ChessBoard* board);
    virtual ~Game();
    int start();
    void setGameManager(GameManager* manager);
    void setCurrentJumps(int currentJumps);
    void setUpperBound(int upperBound);
    
    GameManager* getGameManager();
    ChessBoard* getBoard();
    Knight* getKnight();
    int getCurrentJumps();
    bool canFinishBeforeTheLimit();
protected:
    Game* deepCopy();
    Point* calculateBestCoordinate();
    bool compareCoordinates(Point* a, Point* b);
    int jumpPrice(Point* p);
    void moveKnightTo(Point* p);
    void calculatePossibleCoordinates(vector<Point*> &points, Point* point);
    void addPointIfValid(vector<Point*> &points, int row, int col);
private:
    Knight* mKnight;
    ChessBoard* mBoard;
    int mCurrentJumps;
    int mUpperBound;
    GameManager* mGameManager;
};

#endif	/* GAME_H */

