/* 
 * File:   ChessBoard.h
 * Author: honza
 *
 * Created on October 7, 2015, 1:02 PM
 */

#ifndef CHESSBOARD_H
#define	CHESSBOARD_H

#include "Point.h"

class ChessBoard {
public:
    ChessBoard(int size, int** field, int figureCount);
    virtual ~ChessBoard();
    void removeFigure(Point *p);
    void setPointValue(Point *p, int value);
    int getSize() const;
    int getFigureCount() const;
    int** getField() const;
    bool isOccupied(Point *p) const;
    bool isOccupied(int row, int col) const;
    bool isEmpty() const;
    bool isValidCoordinate(int row, int col) const;
private:
    int mSize;
    int mFigureCount;
    int** mField;
};

#endif	/* CHESSBOARD_H */

