#include "Point.h"

Point::Point(int row, int col) {
    this->mRow = row;
    this->mCol = col;
}

int Point::getCol() {
    return mCol;
}

int Point::getRow() {
    return mRow;
}