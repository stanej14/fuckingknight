/* 
 * File:   GameManager.cpp
 * Author: honza
 * 
 * Created on October 8, 2015, 9:24 PM
 */

#include <stdio.h>
#include "GameManager.h"
#include "Game.h"

GameManager::GameManager() {
}

GameManager::~GameManager() {
    while (!mGameStack.empty()) {
        Game* temp = mGameStack.top();
        mGameStack.pop();
        delete temp;
    }
}

void GameManager::addGame(Game* game) {
    if (game) {
        mGameStack.push(game);
    }
}

Game* GameManager::takeGameFromStack() {
            
    if (mGameStack.empty()) {
        return NULL;
    }

    Game* toReturn = mGameStack.top();
    mGameStack.pop();
    return toReturn;
}

